#!/bin/sh

version=$1

function versions_web {
    curl --silent https://divine.fi.muni.cz/download/ | grep 'divine-[0-9.]*\.tar' \
          | sed 's/^.*divine-\([0-9.]*\)\.tar.*$/\1/'
}

function scriptversion {
    find -maxdepth 1 -type d -name 'divine-*' | sed 's|\./divine-||' | \
        while read d; do
            echo $1 | grep -q "^$d" && echo $d && break
        done
}

function versions {
    versions_web | while read VERS; do
        V=$(scriptversion $VERS)
        if [[ ! -z $V ]]; then
            echo $VERS;
        fi
    done
}

function printHelpAndExit {
    echo "usage: [DIVINE_VERSION]"
    echo "  available versions:"
    versions | while read V; do
        echo "    - $V"
    done
    exit 2
}

if [[ ! -z $version ]] && [[ $version = "-h" ]]; then
    printHelpAndExit
fi

test -z $version && version=$(versions | tail -n1)
script_version=$(scriptversion $version)

if [[ -z $script_version ]]; then
    echo "ERROR: no script for DIVINE version $version"
    printHelpAndExit >&2
fi

echo "building DIVINE $version (script $script_version)"

if [[ $(git status --porcelain | wc -l) -ne 0 ]]; then
    echo "WARNING: you seem to have changes in the repository" >&2
    git status --short
fi

ncpus=$(cat /proc/cpuinfo | grep 'model name' | wc -l)
test -z $ncpus && ncpus=2
resources="-m 12288 -smp $ncpus"
dist=xenial
img=xenial-server-cloudimg-amd64-disk1.img

test -e $img || wget https://cloud-images.ubuntu.com/$dist/current/$img
qemu-img create -o backing_file=$img -f qcow2 divine-$dist-$version.qcow2 20G
qemu-img create -f qcow2 divine-build-$version.qcow2 20G
mkdir -p cloud-config

cat > cloud-config/meta-data <<EOF
instance-id: iid-divine-xenial
local-hostname: divine
EOF

cat > cloud-config/user-data <<EOF
#cloud-config
users:
 - name: divine
   lock_passwd: false
   plain_text_passwd: llvm
   home: /home/divine
   sudo: ALL=(ALL) NOPASSWD:ALL
runcmd:
 - apt-get install -y make
 - mkfs.ext4 /dev/sdb
 - mount /dev/sdb /mnt
 - cd /mnt
 - "curl https://gitlab.fi.muni.cz/paradise/vmutils/raw/master/divine-$script_version/install.sh | env DIVINE_VERSION=$version sh"
 - apt-get remove -y cloud-init
 - poweroff
EOF

genisoimage -V cidata -D -J -joliet-long -o config.iso cloud-config
rm -rf cloud-config

qemu-system-x86_64 -machine accel=kvm -display none -serial stdio \
                   -hda divine-$dist-$version.qcow2 -hdb divine-build-$version.qcow2 \
                   -cdrom config.iso $resources

rm -f config.iso
