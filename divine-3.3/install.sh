#!/bin/sh

set -ex
version=3.3.3
test -z $DIVINE_VERSION || version=$DIVINE_VERSION
llvm_version=3.4.2
prefix=/opt/divine


apt-get install -y libc++-dev libc++abi-dev clang cmake

wget http://releases.llvm.org/$llvm_version/llvm-$llvm_version.src.tar.gz
wget http://releases.llvm.org/$llvm_version/cfe-$llvm_version.src.tar.gz

tar xzf llvm-$llvm_version.src.tar.gz
tar xzf cfe-$llvm_version.src.tar.gz
rm *.tar.gz
mv cfe-$llvm_version.src llvm-$llvm_version.src/tools/clang
cd llvm-$llvm_version.src
./configure --prefix=$prefix --disable-assertions
make -j4
make install
cd ..

wget https://divine.fi.muni.cz/download/divine-$version.tar.gz
tar xzf divine-$version.tar.gz
cd divine-$version

./configure -DCMAKE_INSTALL_PREFIX=$prefix -DLLVM_CONFIG_PATH=$prefix/bin/llvm-config \
    -DCMAKE_EXE_LINKER_FLAGS="-latomic" \
    -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ \
    -DCMD_CLANG=$prefix/bin/clang
#    -DCMAKE_CXX_FLAGS="-stdlib=libc++" -DCMAKE_EXE_LINKER_FLAGS="-lc++abi" \
#    -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++
make -j4
make install

echo "DIVINE's binaries are installed in $prefix/bin"

if test -d /etc/profile.d; then
    echo "PATH=$prefix/bin:"'$PATH' > /etc/profile.d/divine-path.sh
    echo "I have created /etc/profile.d/divine-path.sh to update system PATH"
    echo "After you log out and back in, it should be available as 'divine'"
fi

cat <<EOF >> /etc/issue
DIVINE $version VM

login: divine
password: llvm

EOF
